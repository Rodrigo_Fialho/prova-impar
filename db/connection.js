const { console } = require('console');
const mysql = require('mysql2');

const connection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database: 'cadastro'
});

connection.connect((error)=>{
    if(error)
    console.log(error);


});

module.exports = connection;